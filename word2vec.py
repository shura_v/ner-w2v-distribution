# based on https://russianblogs.com/article/9970486226/
import numpy as np
from collections import defaultdict

class word2vec():

    def __init__(self, settings):
        self.n = settings['n']
        self.lr = settings['learning_rate']
        self.epochs = settings['epochs']  # сколько эпох зарядили сейчас
        self.total_epochs = 0  # сколько эпох всего
        self.window = settings['window_size']
        self.corp_name = settings['corp_name']

    def generate_training_data(self, settings, corpus):
        """
        Получить данные обучения
        """

        # defaultdict (int) Словарь для создания значения по умолчанию с типом int, если доступ к ключу не существует
        word_counts = defaultdict(int)

        # Traversal corpus corpus
        for row in corpus:
            for word in row:
                # Подсчитать количество вхождений каждого слова
                word_counts[word] += 1

        # Длина глоссария
        self.v_count = len(word_counts.keys())
        # Список слов в словаре
        self.words_list = list(word_counts.keys())
        self.words_counts = list(word_counts.values())
        # Словарь данных со словами в словаре в качестве ключа и индексом в качестве значения
        self.word_index = dict((word, i) for i, word in enumerate(self.words_list))
        # Dictionary данных с индексом в качестве ключа и слова в словаре в качестве значения
        self.index_word = dict((i, word) for i, word in enumerate(self.words_list))

        training_data = []

        for sentence in corpus:
            sent_len = len(sentence)

            for i, word in enumerate(sentence):

                w_target = self.word2onehot(sentence[i])

                w_context = []

                for j in range(i - self.window, i + self.window):
                    if j != i and j <= sent_len - 1 and j >= 0:
                        w_context.append(self.word2onehot(sentence[j]))

                training_data.append([w_target, w_context])
        return np.array(training_data)

    def word2onehot(self, word):

        # Кодировать слова с одним снимком

        word_vec = [0 for i in range(0, self.v_count)]

        word_index = self.word_index[word]

        word_vec[word_index] = 1

        return word_vec

    def train(self, training_data, n_epochs=0):

        if n_epochs != 0: self.epochs = n_epochs

        # Изменить параметры w1, w2
        if self.total_epochs == 0:  # 220410 случайно, если неученый, если ученый - останутся те что есть
            self.w1 = np.random.uniform(-1, 1, (self.v_count, self.n))
            self.w2 = np.random.uniform(-1, 1, (self.n, self.v_count))

        for i in range(self.epochs):
            # print('эпоха ',i)

            self.loss = 0

            # w_t - горячий вектор, представляющий целевое слово
            # w_t -> w_target,w_c ->w_context
            for w_t, w_c in training_data:
                # Прямое распространение
                y_pred, h, u = self.forward(w_t)

                # Ошибка расчета
                EI = np.sum([np.subtract(y_pred, word) for word in w_c], axis=0)

                # Обратное распространение, обновление параметров
                self.backprop(EI, h, w_t)

                # Рассчитать общий убыток
                self.loss += -np.sum([u[word.index(1)] for word in w_c]) + len(w_c) * np.log(np.sum(np.exp(u)))

            self.total_epochs += 1
            print('Epoch:', i, "Loss:", self.loss)
        # print(type(self.w1),self.w1)

    def forward(self, x):
        """
                  Прямое распространение
        """

        h = np.dot(self.w1.T, x)

        u = np.dot(self.w2.T, h)

        y_c = self.softmax(u)

        return y_c, h, u

    def softmax(self, x):
        e_x = np.exp(x - np.max(x))

        return e_x / np.sum(e_x)

    def backprop(self, e, h, x):

        d1_dw2 = np.outer(h, e)
        d1_dw1 = np.outer(x, np.dot(self.w2, e.T))

        self.w1 = self.w1 - (self.lr * d1_dw1)
        self.w2 = self.w2 - (self.lr * d1_dw2)

    def word_vec(self, word):

        """
                  Получить слово вектор
                  Найти прямо в векторе веса, получив индекс слова
        """

        w_index = self.word_index[word]
        # print(type(w_index),w_index)

        v_w = self.w1[w_index]
        # print(v_w)

        return v_w

    def vec_sim(self, word, top_n=0):
        """
                  Найти похожие слова
        """

        v_w1 = self.word_vec(word)
        word_sim = {}

        for i in range(self.v_count):
            v_w2 = self.w1[i]
            theta_sum = np.dot(v_w1, v_w2)

            # np.linalg.norm (v_w1) Найти норму По умолчанию используется норма 2, которая является квадратичной степенью суммы квадратов
            theta_den = np.linalg.norm(v_w1) * np.linalg.norm(v_w2)
            theta = theta_sum / theta_den

            word = self.index_word[i]
            word_sim[word] = theta

        words_sorted = sorted(word_sim.items(), key=lambda kv: kv[1], reverse=True)

        if top_n > 0:
            for word, sim in words_sorted[:top_n]:
                print(word, sim)
        return words_sorted

    def get_w(self):
        w1 = self.w1
        return w1

    # для pickle 2 метода
    def __getstate__(self) -> dict:  # Как мы будем "сохранять" класс
        state = {}
        state["config"] = [self.n, self.lr, self.epochs, self.window, self.v_count, self.corp_name, self.total_epochs]
        state["words_list"] = self.words_list
        state["words_counts"] = self.words_counts

        state["w1"] = self.w1
        state["w2"] = self.w2
        state["loss"] = self.loss
        # trainig_data не стал сохранять, можно отдельно

        return state

    def __setstate__(self, state: dict):  # Как мы будем восстанавливать класс из байтов
        # print(len(state["config"]))

        if len(state["config"]) <= 6:
            self.n, self.lr, self.epochs, self.window, self.v_count, self.corp_name = state["config"]
        else:
            self.n, self.lr, self.epochs, self.window, self.v_count, self.corp_name, self.total_epochs = state["config"]
            self.total_epochs = self.epochs
        # print(self.total_epochs)
        # это для первой дозагрузки поменялся словарь пропустил self.n, self.lr, self.epochs, self.window, self.v_count, self.corp_name = state["config"]#это для первой дозагрузки
        # это для первой дозагрузки поменялся словарь пропустил self.total_epochs=self.epochs#это для первой дозагрузки
        self.words_list = state["words_list"]
        self.words_counts = state["words_counts"]
        self.w1 = state["w1"]
        self.w2 = state["w2"]
        self.loss = state["loss"]

        self.word_index = dict((word, i) for i, word in enumerate(self.words_list))
        self.index_word = dict((i, word) for i, word in enumerate(self.words_list))
        # self.args = state["args"]